#!/bin/bash

# Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo "Nombre de archivo.sh=$0"
echo "(Versión sin rm completo de HOME)"
echo " Este script hace un respaldo de todos los archivos que recibe como argumentos (ejemp: .c .sh) de HOME copiandolos a desktop/RESPALDO que despues sera comprimido en un archivo .tar.bz2"
echo "¿ Esta seguro que desea correr este script (s/n) ?"
read resp


if [ $resp == "s" ]
then
	#chmod -w $0
	cd ~/

	# Check the lenguaje by dir
	if [ -d ~/"Desktop" ]
			then
			 echo "Idioma:English"
				if [ -d "Desktop" ]
					then
						echo "Desktop exist"
					else
						 echo "Desktop was created"
						 mkdir Desktop

				fi

			else
				echo "Idioma:Español"
				if [ -d "Escritorio" ]
					then
						echo "Si existe el directorio Escritorio"
					else
						 echo "No Existe Escritorio y ha sido creado"
						 mkdir Escritorio

				fi

	fi

	# Change to desktop dir
	if [ -d ~/"Desktop" ]
			then
  				cd Desktop
  					if [ -d "Backup" ]
						then
						echo "Backup exist"

						else
						 echo "Backup not exist, creating it..."
						 mkdir Backup

					fi
  			else
				cd Escritorio
					if [ -d "Respaldo" ]
						then
						echo "SI existe el directorio respaldo"

						else
						 echo "NO existe el directorio respaldo, crando la carpeta..."
						 mkdir Respaldo
					fi
	fi

	#create the files, just for test
	#eval "touch file{1..4}.{lñ,ñl}"

	# Formats array
	FORMATS=(${@});

	for FORMATS in "${FORMATS[@]}";
	do
			if [ -d ~/"Desktop" ]
				then
	 				find ~ -iname "*$FORMATS" -exec cp -if {} Backup \;
	 			else
	 				find ~ -iname "*$FORMATS" -exec cp -if {} Respaldo \;
	 		fi
	done

	if [ -d ~/"Desktop" ]
			then
				tar -jcvf `date +%d-%m-%Y`.tar.bz2 Backup
			else
				tar -jcvf `date +%d-%m-%Y`.tar.bz2 Respaldo
	fi
	# Reload formats again
	FORMATS=(${@});


	for FORMATS in "${FORMATS[@]}";
	do
			if [ -d ~/"Desktop" ]
				then
					echo "Actual Directory to delete copys:"
					pwd
					echo "Deleting copy files.."
	 				find . -iname "*$FORMATS" -exec rm '{}' \;
	 			# find ~ -iname "*$FORMATS" -exec rm '{}' \;
	 			else
	 				echo "Directorio actual para borrar copias:"
	 				pwd
	 				echo "Borrando copias de los archivos."
	 				find . -iname "*$FORMATS" -exec rm '{}' \;
	 			# find ~ -iname "*$FORMATS" -exec rm '{}' \;
	 		fi
	done
		if [ -d ~/"Desktop" ]
			then
				echo "Deleting Backup temp folder..."
				 rm -r Backup
			else
				echo "Borrando carpeta temporal Respaldo..."
				rm -r Respaldo
		fi

fi

echo "End of Script."